<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->model('mymaps/ReadModel');
    }

	public function index()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        
		$postjson = json_decode(file_get_contents('php://input'),true);
        $action = $postjson['action'];

        //untuk cekLogin
        if($action=="cekLogin"){
            $user = $postjson['username'];
            $pass = md5($postjson['password']);
            
            $sourcedb = $this->load->database('hrd', TRUE);
            $connected = $sourcedb->initialize();
            $resp = $this->ReadModel->cekLogin($user,$pass,$sourcedb);
        }

        //untuk cekLogin
        if($action=="cekAutoLogin"){
            $user = $postjson['username'];
            $tipe = $postjson['tipe'];

            if($tipe=='autologin'){
                $pass = $postjson['password'];
            }else if($tipe=='manuallogin'){
                $pass = md5($postjson['password']);
            }
            
            $sourcedb = $this->load->database('hrd', TRUE);
            $connected = $sourcedb->initialize();
            $resp = $this->ReadModel->cekLogin($user,$pass,$sourcedb);
        }

        //untuk setDefaultLocation
        if($action=="getDefaultLocation"){
            $resp = $this->ReadModel->getDefaultLocation();
        }

        //untuk getLastlocationsales
        if($action=="getLastlocationsales"){

            $sourcedb = $this->load->database('server88', TRUE);
            $connected = $sourcedb->initialize();

            $kdcabang = $postjson['kdcabang'];
            $tanggal = $postjson['tanggal'];
            $resp = $this->ReadModel->getLastlocationsales($kdcabang,$tanggal,$sourcedb );
        }

        //untuk getLastPositionSalesman
        if($action=="getLastPositionSalesman"){
            $kdcabang = $postjson['kdcabang'];
            $kdsalesman = $postjson['kdsalesman'];
            $tanggal = $postjson['tanggal'];
            $resp = $this->ReadModel->getLastPositionSalesman($kdcabang,$kdsalesman,$tanggal);
        }

        //untuk allCounterList
        if($action=="allCounterList"){
            $id = $postjson['id'];
            $src = $postjson['src'];
            $resp = $this->ReadModel->allCounterList($src,$id);
        }

        //untuk getActivitiesSalesman
        if($action=="getActivitiesSalesman"){
            $kdcabang = $postjson['kdcabang'];
            $kdsalesman = $postjson['kdsales'];
            $tanggal = $postjson['tanggal'];
            $resp = $this->ReadModel->getActivitiesSalesman($kdcabang,$kdsalesman,$tanggal);
        }

        //untuk getAllPositionSalesman
        if($action=="getAllPositionSalesman"){
            $kdcabang = $postjson['kdcabang'];
            $kdsalesman = $postjson['kdsalesman'];
            $tanggal = $postjson['tanggal'];
            $resp = $this->ReadModel->getAllPositionSalesman($kdcabang,$kdsalesman,$tanggal);
        }

        //untuk getLastSevenDay
        if($action=="getLastSevenDay"){
            $resp = $this->ReadModel->getLastSevenDay();
        }

        //untuk getListProduk
        if($action=="getListProduk"){
            $sourcedb = $this->load->database('server88', TRUE);
            $src = $postjson['src'];
            $resp = $this->ReadModel->getListProduk($src,$sourcedb);
        }
        //untuk getKoordinatListProduk
        if($action=="getKoordinatListProduk"){
            $sourcedb = $this->load->database('server88', TRUE);
            $pcode = $postjson['pcode'];
            $cabang = $postjson['cabang'];
            $resp = $this->ReadModel->getKoordinatListProduk($pcode,$cabang,$sourcedb);
        }

        //untuk getOutletSalesman
        if($action=="getOutletSalesman"){
            $kdsalesman = $postjson['kdsalesman'];
            $kdcabang = $postjson['kdcabang'];
            $resp = $this->ReadModel->getOutletSalesman($kdsalesman, $kdcabang);
        }

        //untuk getOutletSalesmanNew
        if($action=="getOutletSalesmanNew"){
            $kdsalesman = $postjson['kdsalesman'];
            $kdcabang = $postjson['kdcabang'];
            $resp = $this->ReadModel->getOutletSalesmanNew($kdsalesman, $kdcabang);
        }

        //untuk getNameSalesman
        if($action=="getNameSalesman"){
            $kdcabang = $postjson['kdcabang'];
            $resp = $this->ReadModel->getNameSalesman($kdcabang);
        }

        //untuk allRadiusRectangleOutletsales
        if($action=="allRadiusRectangleOutletsales"){
            $j = $postjson['j'];
            $k = $postjson['k'];
            $l = $postjson['l'];
            $m = $postjson['m'];
            $resp = $this->ReadModel->allRadiusRectangleOutletsales($j,$k,$l,$m);
        }

        //untuk allRadiusRectangleOutletsalesNew
        if($action=="allRadiusRectangleOutletsalesNew"){
            $j = $postjson['j'];
            $k = $postjson['k'];
            $l = $postjson['l'];
            $m = $postjson['m'];
            $resp = $this->ReadModel->allRadiusRectangleOutletsalesNew($j,$k,$l,$m);
        }

        //untuk allRadiusCircleRectangleOutletsales
        if($action=="allRadiusCircleRectangleOutletsales"){
            $j = $postjson['j'];
            $k = $postjson['k'];
            $l = $postjson['l'];
            $m = $postjson['m'];
            $dataListOutlet = $this->ReadModel->allRadiusCircleRectangleOutletsales($j,$k,$l,$m);
           
            $count = $dataListOutlet['result'][0]['count'];
          
            $sourcedb = $this->load->database('server88', TRUE);
            $connected = $sourcedb->initialize();
           
			if (!$connected){
                $dataSalesOutlet = array('success'=>false,'msg'=>'Koneksi Gagal');
                $dataPCodeOutlet = array('success'=>false,'msg'=>'Koneksi Gagal');
    		}else{
                $dataSalesOutlet = $this->ReadModel->getDataSalesOutlet($sourcedb,$dataListOutlet,$count);
                $dataPCodeOutlet = $this->ReadModel->getDataPCodeOutlet($sourcedb,$dataListOutlet,$count);
                $dataSalesEquivalent = $this->ReadModel->getDataSalesEquivalent($sourcedb,$dataListOutlet,$count);// ini belum disesuaikan karena mengerjakan oracle
                $dataSalesKontribusiBrand = $this->ReadModel->getDataSalesKontribusiBrand($sourcedb,$dataListOutlet,$count);// ini juga belum disesuaikan karena mengerjakan oracle
                $dataSalesPerMonth = $this->ReadModel->getDataSalesPerMonth($sourcedb,$dataListOutlet,$count);
            }
            
             $resp= array('success'=>true,
                          'dataListOutlet'=>$dataListOutlet,
                          'data_detail_sales_outlet'=>$dataSalesOutlet,
                          'data_detail_pcode_outlet'=>$dataPCodeOutlet,
                          'data_detail_sales_equivalent'=>$dataSalesEquivalent,
                          'data_detail_sales_kontribusi_brand'=>$dataSalesKontribusiBrand,
                          'data_detail_sales_per_month'=>$dataSalesPerMonth
                        );
        }

        //untuk getAllOutlets
        if($action=="getAllOutlets"){
            $resp = $this->ReadModel->getAllOutlets();
        }

        //untuk getAllCounters
        if($action=="getAllCounters"){
            $category = $postjson['category'];
            $type = $postjson['type'];
            $resp = $this->ReadModel->getAllCounters($category,$type);
        }
        
        //eksekusi lempar daya ke PWA
        json_output($response['status'],$resp);
        
    }

}
