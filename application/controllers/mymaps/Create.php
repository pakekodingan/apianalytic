<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->helper('path');
        $this->load->library('email'); 
        $this->load->model('mymaps/CreateModel');
    }

	public function index()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        
		$postjson = json_decode(file_get_contents('php://input'),true);
        $action = $postjson['action'];

        //createCounter
        if($action=="createCounter"){

            if(!empty($postjson['closing_date'])){
                $cls_date = $postjson['closing_date'];
            }else{
                $cls_date = '';
            }
            $outletname = $postjson['outletname'];
            $address = $postjson['address'];
            $note = $postjson['note'];
            $latitude = $postjson['latitude'];
            $longitude = $postjson['longitude'];
            $opening_date = $postjson['opening_date'];
            $closing_date = $cls_date;
            $category = $postjson['category'];

            $resp = $this->CreateModel->createCounter($outletname,$address,$note,$latitude,$longitude,$opening_date,$closing_date,$category);
        }

        //editCounter
        if($action=="editCounter"){

            if(!empty($postjson['closing_date'])){
                $cls_date = $postjson['closing_date'];
            }else{
                $cls_date = '';
            }
            $id = $postjson['id'];
            $outletname = $postjson['outletname'];
            $address = $postjson['address'];
            $note = $postjson['note'];
            $latitude = $postjson['latitude'];
            $longitude = $postjson['longitude'];
            $opening_date = $postjson['opening_date'];
            $closing_date = $cls_date;
            $category = $postjson['category'];

            $resp = $this->CreateModel->editCounter($id,$outletname,$address,$note,$latitude,$longitude,$opening_date,$closing_date,$category);
        }

        //deleteCounter
        if($action=="deleteCounter"){

            $id = $postjson['id'];
            $resp = $this->CreateModel->deleteCounter($id);
        }

        //exportExcel
        if($action=="exportExcel"){
     
            $data = $postjson['data'];
            $username = $postjson['username'];
            $email = $postjson['email'];
            $sourcedb = $this->load->database('maps', TRUE);
            $this->CreateModel->exportExcel($data,$username,$sourcedb);
            $resp = $this->doThis($email);
            // $resp = $this->CreateModel->expertExcel($data,$sourcedb);
            // $this->load->view('mymaps/exportExcelPlace');
        }

        //eksekusi lempar data ke PWA
        json_output($response['status'],$resp);
        
    }

    public function isinya(){

        $sourcedb = $this->load->database('maps', TRUE);
        $data = $this->CreateModel->getPlace($sourcedb);
        
        $output="";
        $output.="
                <table>
                <tr>
                    <td>
                        Nama Tempat
                    </td>
                    <td>
                        Alamat
                    </td>
                </tr>";

        foreach($data as  $val){
        $output.="
                <tr>
                    <td>
                        ".$val['name']."
                    </td>
                    <td>
                        ".$val['address']."
                    </td>
                </tr>";
        };

        $output.="</table>";
        
        return $output;

        // $data['data'] = $this->CreateModel->getPlace($sourcedb);
        // $output = $this->load->view('mymaps/exportExcelPlace',$data);

    }

    function doThis($email)
    {   
            $response['status'] =200;

            $kg = "";
            $cb = "";
            $tgl = date('d-m-Y');
            $email_to = $email;
            $isiattc    = $this->isinya();// buat attc nya
            $pathDta        = set_realpath(APPPATH."emailtemp");
            $judulattc  = "excel_places_".$cb."_".$kg."_".$tgl;
            $this->createAttach("$judulattc",$isiattc);

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '30';
                                                 
            $config['smtp_user']    = 'herborist2014@gmail.com';
            $config['smtp_pass']    = 'omah2017';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'text'; 
            $config['validation']   = TRUE; 
 
            $this->email->initialize($config);
            
            $this->email->from('noreply@vci.co.id','NoReply Email'); 
            $this->email->to($email_to);  
            $this->email->Subject('Export Excel '.$cb.' '.$kg); 
            $this->email->message("Export Excel : ".$kg." - ".$cb." ".$tgl); 
            $this->email->attach($pathDta.$judulattc.".xls");

              if (!$this->email->send()) {
                $data['msg'] = show_error($this->email->print_debugger()); 
                return array('success'=>true, 'msg'=>'Please Check Your Email.');
              } else {
                 unlink($pathDta.$judulattc.".xls");
                $data['msg'] = "Excel berhasil di kirim";
                return array('success'=>false, 'msg'=>'Sending Email Failed.','error'=>show_error($this->email->print_debugger()));
              }


        

    }
        
        
    function createAttach($subjek,$message){
            $pathDta    = set_realpath(APPPATH."emailtemp");
            $handle = fopen($pathDta.$subjek.".xls", "a");
            fwrite($handle, $message);
            fclose($handle);
    }

}
