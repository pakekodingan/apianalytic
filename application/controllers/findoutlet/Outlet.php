<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->model('findoutlet/OutletModel');
    }

	public function index()
	{ 
        
    }

    public function getoutlet()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $jenis_outlet = $postjson['jenis_outlet'];
        $kdoutlet = $postjson['kdoutlet'];
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        
        if($jenis_outlet=='distributor'){
            $resp = $this->OutletModel->getOutletDistributor($sourcedb,$kdcabang,$kdoutlet,$keyword);
        }else{
            $resp = $this->OutletModel->getOutlet($sourcedb,$kdcabang,$keyword);
        }
        

        json_output($response['status'],$resp);
    }

    public function gettransgocheck()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        $first = $postjson['first'];
        $filter = $postjson['filter'];
        $offset = $postjson['offset'];
        $resp = $this->OutletModel->gettransgocheck($sourcedb,$kdcabang,$keyword,$first,$offset,$filter);

        json_output($response['status'],$resp);
    }

    public function getDistributor()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        $first = $postjson['first'];
        $filter = $postjson['filter'];
        $offset = $postjson['offset'];
        $user_level = $postjson['user_level'];
        $kdoutlet = $postjson['kdoutlet'];
        $resp = $this->OutletModel->getdistributor($sourcedb,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet);

        json_output($response['status'],$resp);
    }

    public function getLogs()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        $first = $postjson['first'];
        $filter = $postjson['filter'];
        $offset = $postjson['offset'];
        $user_level = $postjson['user_level'];
        $kdoutlet = $postjson['kdoutlet'];
        $resp = $this->OutletModel->getLogs($sourcedb,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet);

        json_output($response['status'],$resp);
    }

    public function getDetailOutletDistributor()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        $first = $postjson['first'];
        $filter = $postjson['filter'];
        $offset = $postjson['offset'];
        $user_level = $postjson['user_level'];
        $kdoutlet = $postjson['kdoutlet'];
        $resp = $this->OutletModel->getDetailOutletDistributor($sourcedb,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet);

        json_output($response['status'],$resp);
    }

    public function getListUserDistributor()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdcabang = $postjson['kdcabang'];
        $keyword = $postjson['keyword'];
        $first = $postjson['first'];
        $filter = $postjson['filter'];
        $offset = $postjson['offset'];
        $user_level = $postjson['user_level'];
        $kdoutlet = $postjson['kdoutlet'];
        $resp = $this->OutletModel->getListUserDistributor($sourcedb,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet);

        json_output($response['status'],$resp);
    }

    public function getlistoutlet()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kdsales = $postjson['kdsales'];
        $kdcabang = $postjson['kdcabang'];
        $first = $postjson['first'];
        $offset = $postjson['offset'];
        $keyword = $postjson['keyword'];
        $filter = $postjson['filter'];
        $resp = $this->OutletModel->getlistOutlet($sourcedb,$kdsales,$kdcabang,$keyword,$first,$offset,$filter);

        json_output($response['status'],$resp);
    }

    public function updateoutlet()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $id = $postjson['id'];
        $outlet = $postjson['outlet'];
        $kdcabang = $postjson['kdcabang'];
        $lat = $postjson['lat'];
        $lng = $postjson['lng'];
        $name = $postjson['name'];
        $address = $postjson['address'];
        $notevalidate = $postjson['notevalidate'];
        $marker = $postjson['marker'];
        $status = $postjson['status'];
        $resp = $this->OutletModel->updateOutlet($sourcedb,$id,$outlet,$kdcabang,$lat,$lng,$marker,$name, $address,$notevalidate,$status );

        json_output($response['status'],$resp);
    }


    public function updateoutlet_fromwhatsapp()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);

        $outlet = $postjson['outlet'];
        $kdcabang = $postjson['kdcabang'];
        $lat = $postjson['lat'];
        $lng = $postjson['lng'];
        $name = $postjson['name'];
        $address = $postjson['address'];
        $note = $postjson['note'];
        $notevalidate = $postjson['notevalidate'];
        $marker = $postjson['marker'];
        $status = $postjson['status'];
        $kdsales = $postjson['kdsales'];
        $resp = $this->OutletModel->updateOutletFromWhatsApp($sourcedb,$outlet,$kdcabang,$lat,$lng,$marker,$name, $address,$notevalidate,$status,$kdsales,$note );

        json_output($response['status'],$resp);
    }


    public function savedata()
    {
        $response['status'] =200;
        $ipaddress          = $_SERVER['REMOTE_ADDR'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        
        $postjson = json_decode(file_get_contents('php://input'),true);
        $kode_outlet = $postjson['kode_outlet'];
        $outlet_name = $postjson['outlet_name'];
        $address = $postjson['address'];
        $note = $postjson['note'];
        $latitude = $postjson['latitude'];
        $longitude = $postjson['longitude'];
        $images = $postjson['images'];
        $marker = $postjson['marker'];
        $kdcabang = $postjson['kdcabang'];
        $kdsales = $postjson['kdsales'];
        $resp = $this->OutletModel->savedata($sourcedb,
                                            $kode_outlet,
                                            $outlet_name,
                                            $address,
                                            $note,
                                            $latitude,
                                            $longitude,
                                            $images,
                                            $marker,
                                            $kdcabang,
                                            $kdsales   
                                            );

        json_output($response['status'],$resp);
    }

}
