<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadimage extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

	public function index()
	{
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
		$target_path = "public/outlets/";
        $images =basename($_FILES['file']['name']);
        $target_path = $target_path . $images;
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            return array('success'=>true);
        } else {
            echo "There was an error uploading the file, please try again!";
        }

        } 
        
    }
    
