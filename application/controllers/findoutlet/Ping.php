<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ping extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

	public function index()
	{ 
        $response['status'] =200;
        $resp = array('success'=>true);

        json_output($response['status'],$resp);
    }


}
