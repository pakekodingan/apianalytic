<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');     
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->model('findoutlet/LoginModel');
    }

	public function index()
	{ 
        
    }

    public function cekuser()
	{
        $response['status'] =200;
        $ipaddress		    = $_SERVER['REMOTE_ADDR'];
        
		$postjson = json_decode(file_get_contents('php://input'),true);
        $username = $postjson['username'];
        $password = md5($postjson['password']);
        $platform = $postjson['platform'];
        $sourcedb = $this->load->database('serverGoCheck', TRUE);
        //distributor
        if(substr($username, 0, 2)=="d_"){
           
            $resp = $this->LoginModel->cekUserDistributor($sourcedb,$username,$password,$platform);
        }else{
            $resp = $this->LoginModel->cekUser($sourcedb,$username,$password,$platform);
        }
        

        json_output($response['status'],$resp);
    }

}
