<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CreateModel extends CI_Model {

	function createCounter($outletname,$address,$note,$latitude,$longitude,$opening_date,$closing_date,$category){

			$data = array(
							'namacounter'=>$outletname,
							'address'=>$address,
							'notes'=>$note,
							'lat'=>$latitude,
							'lng'=>$longitude,
							'open_date'=>$opening_date,
							'close_date'=>$closing_date,
							'c_category'=>$category
						);
		    $check = $this->db->insert('counter',$data);
			
			if($check){
				return array('Info'=>true, 'msg'=>'Create New Counter Successfully.');
			}else{
				return array('Failed'=>false, 'msg'=>'Create New Counter Failed.');
			}
		
	}

	function editCounter($id,$outletname,$address,$note,$latitude,$longitude,$opening_date,$closing_date,$category){

			$data = array(
							'namacounter'=>$outletname,
							'address'=>$address,
							'notes'=>$note,
							'lat'=>$latitude,
							'lng'=>$longitude,
							'open_date'=>$opening_date,
							'close_date'=>$closing_date,
							'c_category'=>$category
						);
		    $check = $this->db->update('counter',$data,array('id'=>$id));
			
			if($check){
				return array('Info'=>true, 'msg'=>'Edit Counter Successfully.');
			}else{
				return array('Failed'=>false, 'msg'=>'Edit Counter Failed.');
			}
		
	}


	function deleteCounter($id){

			$check = $this->db->delete('counter',array('id'=>$id));
			if($check){
				return array('Info'=>true, 'msg'=>'Delete Counter Successfully.');
			}else{
				return array('Failed'=>false, 'msg'=>'Delete Counter Failed.');
			}
		
	}


	function exportExcel($data,$username,$conn){

		$conn->delete('places',array('username'=>$username));

		$pisah = explode("##", $data);

            foreach ($pisah as $s) { // each part
              if ($s) {
                $pisah2 = explode("#", $s);
                $name = $pisah2[0];
                $address =  $pisah2[1];

                $isi = array(
                		'name'=>$name,
                		'address'=>$address,
                		'username'=>$username
                		);
                $conn->insert('places',$isi);
              }
            }
            return array('success'=>true, 'msg'=>'Export Excel Successfully.');
	}


	function getPlace($conn){
		$sql = "SELECT * FROM places";
		$qry = $conn->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;

	}
	

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

}

