<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

	function cekUser($conn,$username,$password,$platform)
    {
         $sql = "
				SELECT
				ed.`employee_id`,
				ed.`employee_name`,
				ed.`jabatan_name`,
				ed.`cabang_id`,
				ed.`cabang_name`,
				ed.`username`,
				u.`password`
				FROM
				hrd.employee_dashboard ed
				INNER JOIN hrd.`user` u
					ON ed.`username` = u.`username`
				WHERE 1
				AND ed.`v_status` = 'join'
				AND ed.`username`='$username'
				AND u.password='$password';
				";    
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(!empty($row)){

         foreach($row AS $val){

			        $zql = "
					SELECT u.`Kode` FROM `user` u WHERE u.UserName='$username';
					";    
					$kry = $this->db->query($zql);
					$rou = $kry->result_array();
		            if(!empty($rou)){
						$kodesales = $rou[0]['Kode'];
					}else{
						$kodesales = 'VCI001';
					}

					if($val['cabang_id']=='2'){
						$cabang_id = '33';
					}else if($val['cabang_id']=='3'){
						$cabang_id = '43';
					}else if($val['cabang_id']=='4'){
						$cabang_id = '31';
					}else if($val['cabang_id']=='5'){
						$cabang_id = '32';
					}else if($val['cabang_id']=='9'){
						$cabang_id = '42';
					}else if($val['cabang_id']=='10'){
						$cabang_id = '41';
					}else if($val['cabang_id']=='13'){
						$cabang_id = '31';
					}

					//user level
					$zkl = "
					SELECT * FROM `user_privillage` u WHERE u.username='$username';
					";    
					$kly = $conn->query($zkl);
					$rous = $kly->result_array();

					if(!empty($rous)){
						$user_level = $rous[0]['userlevel'];
					}else{
						//sales biasa
						$user_level = 4;
					}

					$data[] = array('employee_name'=>$val['employee_name'],
									'jabatan_name'=>$val['jabatan_name'],
									'cabang_id'=>$cabang_id,
									'cabang_name'=>$val['cabang_name'],
									'username'=>$username,
									'password'=>$password,
									'platform'=>$platform,
									'kdsales'=>$kodesales,
									'kdoutlet'=>'',
									'jenis_outlet'=>'VCI',
									'user_level'=>$user_level
								);
				
			};
			
			//masuk ke log
			$data_log = array(
								'user'=>$username, 
								'note'=>'Login', 
								'platform'=>$platform,
								'date_log'=>date('Y-m-d H:i:s')
							 );
			$conn->insert('gocheck_log',$data_log);

			return array('success'=>true, 'msg'=>'Ok','result'=>$data);
	

		}else{
			return array('success'=>false, 'msg'=>'Access Denied');
		}
	}


	function cekUserDistributor($conn,$username,$password,$platform)
    {
         $sql = "
				SELECT
				u.`name` AS employee_name,
				u.`jabatan` AS jabatan_name,
				u.`kdsales`,
				u.`kdoutlet`,
				u.`KdCabang` AS cabang_id,
				c.`NamaCabang` AS cabang_name,
				'distributor' AS jenis_outlet,
				u.`username`,
				u.`password`
				FROM
				`user_distributor` u
				INNER JOIN `outlet` o
					ON u.`kdoutlet` = o.`KdOutlet`
					AND u.`kdcabang` = o.`KdCabang`
				INNER JOIN cabang c
					ON o.`KdCabang` = c.`KdCabang`
				WHERE u.`username` = '$username'
				AND u.`password` = '$password'
				AND u.`status`='A'
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
        if(!empty($row)){

			

         foreach($row AS $val){

					//user level
					$zkl = "
					SELECT * FROM `user_privillage` u WHERE u.username='$username';
					";    
					$kly = $conn->query($zkl);
					$rous = $kly->result_array();

					if(!empty($rous)){
						$user_level = $rous[0]['userlevel'];
					}else{
						//sales biasa
						$user_level = 4;
					}

					$data[] = array('employee_name'=>$val['employee_name'],
									'jabatan_name'=>$val['jabatan_name'],
									'cabang_id'=>$val['cabang_id'],
									'cabang_name'=>$val['cabang_name'],
									'username'=>$username,
									'password'=>$password,
									'platform'=>$platform,
									'kdsales'=>$val['kdsales'],
									'kdoutlet'=>$val['kdoutlet'],
									'jenis_outlet'=>$val['jenis_outlet'],
									'user_level'=>$user_level
								);
				
			};
			
			//masuk ke log
			$data_log = array(
								'user'=>$username, 
								'note'=>'Login', 
								'platform'=>$platform,
								'date_log'=>date('Y-m-d H:i:s')
							 );
			$conn->insert('gocheck_log',$data_log);

			return array('success'=>true, 'msg'=>'Ok','result'=>$data);

		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'Access Denied');
		}
	}

	
}

