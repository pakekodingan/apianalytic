<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OutletModel extends CI_Model {

	function getOutlet($conn,$kdcabang,$keyword){
		
		$sql = "
				SELECT * FROM outlet WHERE Nama LIKE '%$keyword%' AND KdCabang='$kdcabang' ORDER BY Nama ASC LIMIT 50;
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array('KdOutlet'=>$val['KdOutlet'],
									'Nama'=>$val['Nama'],
									'Alm1Toko'=>$val['Alm1Toko']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getOutletDistributor($conn,$kdcabang,$kdoutlet,$keyword){
		
		$sql = "
		        SELECT * FROM outlet_detail WHERE Nama LIKE '%$keyword%' AND KdCabang='$kdcabang' AND KdDistributor='$kdoutlet' ORDER BY Nama ASC LIMIT 50;
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array('KdOutlet'=>$val['KdOutlet'],
									'Nama'=>$val['Nama'],
									'Alm1Toko'=>$val['Alm1Toko']. ' ' . $val['KotaToko']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getTransGoCheck($conn,$kdcabang,$keyword,$first,$offset,$filter){

		if($keyword==""){
			$keyword="";
		}else{
			$keyword=" AND (OutletName LIKE '%$keyword%' OR KdOutlet LIKE '%$keyword%') ";
		}

		if($filter=='all'){
			$filter = "";
		}else if($filter=='pending'){
			$filter = " AND Status='0' ";
		}else if($filter=='validate'){
			$filter = " AND Status='1' ";
		}else if($filter=='reject'){
			$filter = " AND Status='2' ";
		}
		$sql = "
				SELECT * FROM trans_gocheck WHERE 1 $keyword $filter AND KdCabang='$kdcabang' ORDER BY id ASC LIMIT $first,$offset;
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array(
						            'id'=>$val['id'],
						            'KdOutlet'=>$val['KdOutlet'],
									'OutletName'=>$val['OutletName'],
									'Address'=>$val['Address'],
									'UserUpload'=>$val['UserUpload'],
									'DateUpload'=>$val['DateUpload'],
									'UserValidate'=>$val['UserValidate'],
									'DateValidate'=>$val['DateValidate'],
									'Lat'=>$val['Lat'],
									'Lng'=>$val['Lng'],
									'Status'=>$val['Status'],
									'Images'=>$val['Images'],
									'TypeData'=>$val['TypeData'],
									'Note'=>$val['Note'],
									'NoteValidator'=>$val['NoteValidator']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getdistributor($conn,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet){

		if($user_level=='1' OR $user_level=='-1'){
			$kdcabang = '';
			$kdoutlet='';
		}else{
			$kdcabang = " AND o.KdCabang = '$kdcabang'";
			$kdoutlet=" AND o.`KdOutlet` = '$kdoutlet'";
		}

		if($keyword==""){
			$keyword="";
		}else{
			$keyword=" AND (o.user LIKE '%$keyword%' OR o.note LIKE '%$keyword%') ";
		}

		if($filter=='all'){
			$filter = "";
		}

		$sql = " 
				
					SELECT
					o.`KdOutlet`,
					o.`Nama`,
					o.`KotaToko`,
					o.`KdCabang`
					FROM
					outlet o
					INNER JOIN outlet_detail od
						ON o.`KdOutlet` = od.`KdDistributor`
						AND o.`KdCabang` = od.`KdCabang`
					WHERE 1 $kdcabang $kdoutlet $filter $keyword
					GROUP BY o.`KdOutlet`
					ORDER BY od.`Nama` ASC
					LIMIT $first,$offset";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array(
						            'KdOutlet'=>$val['KdOutlet'],
									'Nama'=>$val['Nama'],
									'KdCabang'=>$val['KdCabang'],
									'KotaToko'=>$val['KotaToko']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getLogs($conn,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet){

		if($keyword==""){
			$keyword="";
		}else{
			$keyword=" AND (gl.user LIKE '%$keyword%' OR gl.note LIKE '%$keyword%') ";
		}

		$sql = "
				SELECT * FROM gocheck_log gl WHERE 1 $keyword ORDER BY id DESC LIMIT $first,$offset;";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array(
						            'username'=>$val['user'],
									'note'=>$val['note'],
									'platform'=>$val['platform'],
									'date_log'=>$val['date_log']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getDetailOutletDistributor($conn,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet){

		if($keyword==""){
			$keyword="";
		}else{
			$keyword=" AND (od.Nama LIKE '%$keyword%' OR od.KdOutlet LIKE '%$keyword%') ";
		}

		if($filter=='all'){
			$filter = "";
		}

		$sql = "
				SELECT
				od.`KdOutlet`,
				od.`Nama`,
				od.`KotaToko`
				FROM
				outlet o
				INNER JOIN outlet_detail od
					ON o.`KdOutlet` = od.`KdDistributor`
					AND o.`KdCabang` = od.`KdCabang`
				WHERE 1 $filter $keyword
				AND o.KdCabang = '$kdcabang'
				AND o.`KdOutlet` = '$kdoutlet'
				ORDER BY od.`Nama` ASC
				LIMIT $first,$offset;";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array(
						            'KdOutlet'=>$val['KdOutlet'],
									'Nama'=>$val['Nama'],
									'KotaToko'=>$val['KotaToko']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}


	function getListUserDistributor($conn,$kdcabang,$keyword,$first,$offset,$filter,$user_level,$kdoutlet){

		if($keyword==""){
			$keyword="";
		}else{
			$keyword=" AND (ud.name LIKE '%$keyword%' OR ud.kdsales LIKE '%$keyword%') ";
		}

		if($filter=='all'){
			$filter = "";
		}

		$sql = "
				SELECT
				*
				FROM
				`user_distributor` ud
				WHERE 1 $filter $keyword
				AND ud.`kdcabang` = '$kdcabang'
				AND ud.`kdoutlet` = '$kdoutlet'
				ORDER BY ud.`name` ASC
				LIMIT $first,$offset;
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array(
						            'username'=>$val['username'],
									'name'=>$val['name'],
									'kdsales'=>$val['kdsales']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function getlistOutlet($conn,$kdsales,$kdcabang,$keyword,$first,$offset,$filter){

		if($keyword==''){
			$keyword='';
		}else{
			$keyword= " AND ( OutletName LIKE '%$keyword%' OR KdOutlet LIKE '%$keyword%' )";
		}

		if($filter=='all'){
			$filter = "";
		}else if($filter=='pending'){
			$filter = " AND Status='0' ";
		}else if($filter=='validate'){
			$filter = " AND Status='1' ";
		}else if($filter=='reject'){
			$filter = " AND Status='2' ";
		}

		$sql = "
				SELECT * FROM trans_gocheck a WHERE 1 $keyword $filter AND a.KdCabang='$kdcabang' AND KdSales='$kdsales' ORDER BY a.id ASC LIMIT $first,$offset;
				";    
        $qry = $conn->query($sql);
        $row = $qry->result_array();
		
		if(!empty($row)){
		
            foreach($row AS $val){

					$data[] = array('id'=>$val['id'],
						            'KdOutlet'=>$val['KdOutlet'],
									'OutletName'=>$val['OutletName'],
									'Address'=>$val['Address'],
									'UserUpload'=>$val['UserUpload'],
									'DateUpload'=>$val['DateUpload'],
									'UserValidate'=>$val['UserValidate'],
									'DateValidate'=>$val['DateValidate'],
									'Lat'=>$val['Lat'],
									'Lng'=>$val['Lng'],
									'Status'=>$val['Status'],
									'Images'=>$val['Images'],
									'TypeData'=>$val['TypeData'],
									'Note'=>$val['Note'],
									'NoteValidator'=>$val['NoteValidator']
								);
				
            };
		    return array('success'=>true, 'msg'=>'Ok','result'=>$data);
		}else{
			return array('success'=>false, 'msg'=>'No Found','count'=>0);
		}	
		
	}

	function updateOutlet($conn,$id,$outlet,$kdcabang,$lat,$lng,$marker,$name, $address,$notevalidate,$status ){

		$data=array(
			'Status'=>$status ,
			'NoteValidator'=>$notevalidate,
			'UserValidate'=>$marker,
			'DateValidate'=>date('Y-m-d H:i:s')
		);

		$where = array(
			'id' => $id
		);

		
		$conn->update('trans_gocheck',$data,$where);
		return array('success'=>true,'subtitle'=>'Successfully', 'msg'=> 'Successfully for '.$name);
		
	}

	function updateOutletFromWhatsApp($conn,$outlet,$kdcabang,$lat,$lng,$marker,$name, $address,$notevalidate,$status,$kdsales,$note ){

		$data=array(
				'KdOutlet'=>$outlet,
			    'OutletName'=>$name,
			    'Address'=>$address,
			    'KdCabang'=>$kdcabang,
			    'KdSales'=>$kdsales,
			    'Images'=>'bg.jpg',
			    'Lat'=>$lat,
			    'Lng'=>$lng,
			    'Note'=>$note,
			    'NoteValidator'=>$notevalidate,
			    'Status'=>$status,
			    'TypeData'=>'W',
			    'DateUpload'=>date('Y-m-d H:i:s'),
				'UserUpload'=>$marker,
			    'DateValidate'=>date('Y-m-d H:i:s'),
				'UserValidate'=>$marker,
			
			);

				$conn->insert('trans_gocheck',$data);
                return array('success'=>true,'subtitle'=>'Successfully', 'msg'=>'Outlet Is Added');
		
	}

	function savedata($conn,
                      $kode_outlet,
                      $outlet_name,
                      $address,
                      $note,
                      $latitude,
                      $longitude,
                      $images,
                      $marker,
                      $kdcabang,
                      $kdsales 

                      ){

				$data=array(
				'KdOutlet'=>$kode_outlet,
			    'OutletName'=>$outlet_name,
			    'Address'=>$address,
			    'KdCabang'=>$kdcabang,
			    'KdSales'=>$kdsales,
			    'Images'=>$images,
			    'Lat'=>$latitude,
			    'Lng'=>$longitude,
			    'Note'=>$note,
			    'Status'=>0,
			    'DateUpload'=>date('Y-m-d H:i:s'),
			    'UserUpload'=>$marker);

				$conn->insert('trans_gocheck',$data);
                return array('success'=>true,'subtitle'=>'Successfully', 'msg'=>'Outlet Is Added');

		
		
	}

}

